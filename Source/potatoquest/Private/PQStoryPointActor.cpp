﻿#include "PQStoryPointActor.h"

#include "PQGameState.h"
#include "PQStoryChoiceActor.h"
#include "Camera/CameraActor.h"

APQStoryPointActor::APQStoryPointActor()
	: SceneCamera(nullptr)
{
	CreateDefaultSubobject<USceneComponent>(TEXT("DefaultSceneRoot"));
}

void APQStoryPointActor::BeginPlay()
{
	Disable();

	Super::BeginPlay();
}

void APQStoryPointActor::Enable()
{
	for(APQStoryChoiceActor* Choice : Choices)
	{
		if(IsValid(Choice))
		{
			Choice->Enable(this);
		}
	}

	if(HasActorBegunPlay())
	{
		if(APQGameState* PQGameState = Cast<APQGameState>(GetWorld()->GetGameState()))
		{
			PQGameState->OnStoryPointEnabled(this);
		}
	}

	OnEnable();
}

void APQStoryPointActor::OnEnable_Implementation()
{
	APlayerController* PC = GetWorld()->GetFirstPlayerController();
	PC->SetViewTarget(SceneCamera); // ensure we are at the story point camera by this point

	GetRootComponent()->SetVisibility(true, true);
}

void APQStoryPointActor::Disable()
{
	for(APQStoryChoiceActor* Choice : Choices)
	{
		if(IsValid(Choice))
		{
			Choice->Disable();
		}
	}

	if(HasActorBegunPlay())
	{
		if(APQGameState* PQGameState = Cast<APQGameState>(GetWorld()->GetGameState()))
		{
			PQGameState->OnStoryPointDisabled(this);
		}
	}
	
	OnDisable();
}

void APQStoryPointActor::OnDisable_Implementation()
{
	CameraBlendTimerHandle.Invalidate();
	
	GetRootComponent()->SetVisibility(false, true);
}
