// Fill out your copyright notice in the Description page of Project Settings.

#include "PQGameState.h"

APQGameState::APQGameState()
	: StoryPathOffset(0)
	, PendingStoryPathOffset(0)
{
}

void APQGameState::OnStoryPointDisabled_Implementation(APQStoryPointActor* StoryPoint)
{
	OnStoryPointDisabledSignal.Broadcast(StoryPoint);
}

void APQGameState::OnStoryPointEnabled_Implementation(APQStoryPointActor* StoryPoint)
{	
	if(PendingStoryPathOffset == StoryPathOffset)
	{
		if(StoryPathOffset < 0)
		{
			const int OldPathStartIndex = RecordedStoryPath.Num() + StoryPathOffset;
			const int32 Count = RecordedStoryPath.Num() - OldPathStartIndex;
			RecordedStoryPath.RemoveAt(OldPathStartIndex, Count);
			StoryPathOffset = PendingStoryPathOffset = StoryPathOffset + Count;
		}
		
		RecordedStoryPath.Add(StoryPoint);
	}
	
	OnStoryPointEnabledSignal.Broadcast(StoryPoint);
}
bool APQGameState::MoveStoryPointOffset(const int Count)
{
	bool bSuccess = false;
	const int32 TargetIndex = RecordedStoryPath.Num() + StoryPathOffset + Count - 1;
	if(0 <= TargetIndex && TargetIndex < RecordedStoryPath.Num())
	{
		const int32 CurrentIndex = RecordedStoryPath.Num() + StoryPathOffset - 1;
		APQStoryPointActor* CurrentSP = RecordedStoryPath[CurrentIndex];
		APQStoryPointActor* TargetSP = RecordedStoryPath[TargetIndex];

		PendingStoryPathOffset = StoryPathOffset + Count;
		
		CurrentSP->Disable();
		TargetSP->Enable();
		
		StoryPathOffset = PendingStoryPathOffset;
		bSuccess = true;
	}
	return bSuccess;
}
