// Fill out your copyright notice in the Description page of Project Settings.

#include "PQStoryChoiceActor.h"

#include "PQStoryPointActor.h"
#include "PQStoryTransition.h"
#include "Components/BoxComponent.h"

APQStoryChoiceActor::APQStoryChoiceActor()
	: CollisionMesh(nullptr)
	, TransitionTime(1.0f)
	, EnablingStoryPoint(nullptr)
	, ChoiceStoryPoint(nullptr)
{
	UStaticMesh* StaticMesh = LoadObject<UStaticMesh>(nullptr, TEXT("/Engine/BasicShapes/Cube.Cube"));
	check(StaticMesh != nullptr);
	
	CollisionMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("CollisionMesh"));
	check(CollisionMesh != nullptr);
	SetRootComponent(CollisionMesh);

	CollisionMesh->SetHiddenInGame(true);
	CollisionMesh->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	CollisionMesh->SetCollisionResponseToAllChannels(ECR_Ignore);
	CollisionMesh->SetCollisionResponseToChannel(ECC_Visibility, ECR_Block);
	
	CollisionMesh->SetGenerateOverlapEvents(false);
	CollisionMesh->SetCanEverAffectNavigation(false);
	CollisionMesh->bCastDynamicShadow = false;
	CollisionMesh->bCastStaticShadow = false;
	CollisionMesh->bAffectDistanceFieldLighting = false;
	CollisionMesh->bAffectDynamicIndirectLighting = false;

	TransitionClass = LoadObject<UClass>(nullptr, TEXT("/Game/Game/StoryTransitions/BP_STDefault.BP_STDefault_C"));
}

void APQStoryChoiceActor::Enable(APQStoryPointActor* StoryPoint)
{
	EnablingStoryPoint = StoryPoint;
	SetActorEnableCollision(ECollisionEnabled::QueryOnly);
}

void APQStoryChoiceActor::OnEnable_Implementation()
{
}

void APQStoryChoiceActor::Disable()
{
	SetActorEnableCollision(ECollisionEnabled::NoCollision);
	EnablingStoryPoint = nullptr;
}

void APQStoryChoiceActor::OnDisable_Implementation()
{
}

void APQStoryChoiceActor::NotifyActorOnClicked(FKey ButtonPressed)
{
	Super::NotifyActorOnClicked(ButtonPressed);
	
	OnChoiceClicked(ButtonPressed);
}

void APQStoryChoiceActor::OnChoiceClicked_Implementation(FKey ButtonPressed)
{
	if(false == IsValid(ChoiceStoryPoint))
	{
		// no point to transition to
		return;
	}
	
	if(IsValid(EnablingStoryPoint))
	{
		EnablingStoryPoint->Disable(); // this will disable this choice
	}

	bool bIsCompleteTimerSet = false;
	UObject* DefaultObject = TransitionClass.GetDefaultObject();
	if(nullptr != DefaultObject)
	{
		check(DefaultObject->Implements<UPQStoryTransitionInterface>());
		
		IPQStoryTransitionInterface::Execute_DoTransition(DefaultObject, this);

		if(TransitionTime > 0.0f)
		{
			bIsCompleteTimerSet = true;
			GetWorld()->GetTimerManager().SetTimer(TransitionHandle, this, &APQStoryChoiceActor::OnTransitionComplete, TransitionTime);
		}
	}
	
	if(false == bIsCompleteTimerSet)
	{
		OnTransitionComplete();
	}
}
void APQStoryChoiceActor::OnTransitionComplete()
{
	if(IsValid(ChoiceStoryPoint))
	{
		ChoiceStoryPoint->Enable();
	}
}

