// Copyright Epic Games, Inc. All Rights Reserved.

#include "PotatoQuest.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, PotatoQuest, "PotatoQuest" );
