﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "PQStoryTransitionInterface.h"
#include "PQStoryTransition.generated.h"


UCLASS(Abstract, Blueprintable, meta = (ShowWorldContextPin))
class UPQStoryTransition : public UObject, public IPQStoryTransitionInterface
{
	GENERATED_BODY()
public:
};