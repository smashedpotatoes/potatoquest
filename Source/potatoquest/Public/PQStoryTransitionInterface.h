﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "PQStoryTransitionInterface.generated.h"

class APQStoryChoiceActor;

UINTERFACE(MinimalAPI, Blueprintable)
class UPQStoryTransitionInterface : public UInterface
{
	GENERATED_BODY()
};

class IPQStoryTransitionInterface
{
	GENERATED_BODY()
public:
	UFUNCTION(BlueprintNativeEvent)
	void DoTransition(APQStoryChoiceActor* ChoiceActor);
};