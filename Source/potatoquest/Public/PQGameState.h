// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "PQStoryPointActor.h"
#include "GameFramework/GameStateBase.h"
#include "PQGameState.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FStoryPointSignal, APQStoryPointActor*, StoryPoint);

/**
 * 
 */
UCLASS()
class POTATOQUEST_API APQGameState : public AGameStateBase
{
	GENERATED_BODY()
public:
	APQGameState();
	
	// Called from APQStoryPointActor
	UFUNCTION(BlueprintNativeEvent)
	void OnStoryPointEnabled(APQStoryPointActor* StoryPoint);
	
	// Called from APQStoryPointActor
	UFUNCTION(BlueprintNativeEvent)
	void OnStoryPointDisabled(APQStoryPointActor* StoryPoint);

	UFUNCTION(BlueprintCallable)
	bool MoveStoryPointOffset(const int32 Count = -1);

public:
	UPROPERTY(BlueprintAssignable)
	FStoryPointSignal OnStoryPointEnabledSignal;
	
	UPROPERTY(BlueprintAssignable)
	FStoryPointSignal OnStoryPointDisabledSignal;

	UPROPERTY(BlueprintReadOnly)
	TArray<APQStoryPointActor*> RecordedStoryPath;

	int32 StoryPathOffset;
	int32 PendingStoryPathOffset;
};
