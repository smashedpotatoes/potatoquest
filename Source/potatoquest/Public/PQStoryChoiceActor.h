// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"


#include "PQStoryTransitionInterface.h"
#include "GameFramework/Actor.h"
#include "PQStoryChoiceActor.generated.h"

class APQStoryPointActor;

UCLASS()
class POTATOQUEST_API APQStoryChoiceActor : public AActor
{
	GENERATED_BODY()
public:
	APQStoryChoiceActor();

	UFUNCTION(BlueprintCallable)
	void Enable(APQStoryPointActor* StoryPoint);

	UFUNCTION(BlueprintNativeEvent)
	void OnEnable();

	UFUNCTION(BlueprintCallable)
	void Disable();

	UFUNCTION(BlueprintNativeEvent)
	void OnDisable();

	UStaticMeshComponent* GetCollisionMesh() const { return CollisionMesh; }
	APQStoryPointActor* GetEnablingStoryPoint() const { return EnablingStoryPoint; }
	APQStoryPointActor* GetChoiceStoryPoint() const { return ChoiceStoryPoint; }

protected:
	void NotifyActorOnClicked(FKey ButtonPressed) override;

	UFUNCTION(BlueprintNativeEvent)
	void OnChoiceClicked(FKey ButtonPressed);
	
	UFUNCTION()
	void OnTransitionComplete();

protected:
	UPROPERTY(BlueprintReadOnly, EditAnywhere)
	UStaticMeshComponent* CollisionMesh;
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	float TransitionTime;
	
	UPROPERTY(EditAnywhere, meta = (MustImplement = PQStoryTransitionInterface))
	TSubclassOf<UObject> TransitionClass;

	UPROPERTY(BlueprintReadOnly)
	APQStoryPointActor* EnablingStoryPoint;
	
	UPROPERTY(BlueprintReadOnly, EditAnywhere)
	APQStoryPointActor* ChoiceStoryPoint;

	FTimerHandle TransitionHandle;
};
