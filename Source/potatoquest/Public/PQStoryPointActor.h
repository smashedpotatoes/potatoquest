﻿// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "PQStoryPointActor.generated.h"

class ACameraActor;
class APQStoryChoiceActor;

/**
* 
*/
UCLASS()
class APQStoryPointActor : public AActor
{
	GENERATED_BODY()
public:
	APQStoryPointActor();

	void BeginPlay() override;

	UFUNCTION(BlueprintCallable)
	void Enable();

	UFUNCTION(BlueprintNativeEvent)
	void OnEnable();
	
	UFUNCTION(BlueprintCallable)
	void Disable();
	
	UFUNCTION(BlueprintNativeEvent)
	void OnDisable();

	ACameraActor* GetSceneCamera() const { return SceneCamera; }
	const FText& GetQuestionText() const { return QuestionText; }


protected:	
	UPROPERTY(BlueprintReadOnly, EditAnywhere)
	FText QuestionText;
	
	UPROPERTY(BlueprintReadOnly, EditAnywhere)
	ACameraActor* SceneCamera;
	
	UPROPERTY(BlueprintReadOnly, EditAnywhere)
	TArray<APQStoryChoiceActor*> Choices;

private:
	FTimerHandle CameraBlendTimerHandle;
};
