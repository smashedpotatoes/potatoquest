﻿#pragma once

#include "Blueprint/UserWidget.h"
#include "PQStoryWidget.generated.h"

class APQStoryPointActor;

UCLASS()
class UPQStoryWidget : public UUserWidget
{
	GENERATED_BODY()
public:
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable)
	void Configure(APQStoryPointActor* StoryPoint);
};
