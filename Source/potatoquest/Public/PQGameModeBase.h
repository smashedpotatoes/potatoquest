// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "PQGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class POTATOQUEST_API APQGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
};
